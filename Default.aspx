﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <table border="0" align="center" style="background-color: whitesmoke; border: 1px solid; width: 50%; text-align: center">
                <tr>
                    <td style="font-size: x-large;"><strong>Crud Application</strong></td>
                </tr>
                <tr>
                    <td style="height: 30px;"></td>
                </tr>

                <tr>
                    <td>
                        <table border="0" style="width: 100%;">
                            <tr>
                                <td style="text-align: right; width: 40%;">Name :
                                </td>
                                <td style="width: 60%; text-align: left;">
                                    <asp:TextBox ID="txtName" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                        Display="Dynamic" ErrorMessage="This field is required." SetFocusOnError="True"
                                        ValidationGroup="1" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">Mobile :
                                </td>
                                <td style="text-align: left;">
                                    <asp:TextBox ID="txtMobile" runat="server" Width="200px" MaxLength="12"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMobile"
                                        Display="Dynamic" ErrorMessage="This field is required." Font-Size="10px" ForeColor="Red"
                                        SetFocusOnError="True" ValidationGroup="1"></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMobile"
                                    ErrorMessage="Please Enter Only Numbers" ValidationGroup="1" ForeColor="Red" ValidationExpression="^\d+$"> </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center; height: 20px;"></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnSave" runat="server" Text="Submit" BackColor="#00cc00" ForeColor="White" Width="60px" OnClick="btnSave_OnClick" ValidationGroup="1" />&nbsp;&nbsp;
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" BackColor="#ff0000" ForeColor="White" Width="60px" OnClick="btnReset_OnClick" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center; height: 30px;"></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" >
                                    <asp:GridView ID="grddisplay" runat="server" AutoGenerateColumns="false" HeaderStyle-BackColor="#33cc33" width="90%" 
                                        OnRowCommand="grddisplay_RowCommand" OnRowEditing="grddisplay_RowEditing" OnRowDeleting="grddisplay_RowDeleting"
                                         EmptyDataText="Data is not Available" ItemStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sl.No" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate  >
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                                
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' ></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="edit" CommandArgument='<%# Eval("Id") %>'
                                                        ValidationGroup="2">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="delete" CommandArgument='<%# Eval("Id") %>'
                                                        ValidationGroup="2" OnClientClick="if(confirm('Are you sure you want to Delete?')==false){return false;}">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
       
    </form>
</body>
</html>
