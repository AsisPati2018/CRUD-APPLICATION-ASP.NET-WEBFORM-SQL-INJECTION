﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnection"].ToString());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblMessage.Text = "";
            bind();
        }
    }
    public void bind()
    {
        try
        {
            SqlCommand cmd = new SqlCommand("select * from  Registration order by Name ASC", con);
            SqlDataAdapter sda = new SqlDataAdapter();
            con.Open();
            sda.SelectCommand = cmd;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            grddisplay.DataSource = dt;
            grddisplay.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Something Bad Happened";
        }
    }
    protected void btnSave_OnClick(object sender, EventArgs e)
    {
        try
        {
            if (Session["Edit"] != null)
            {
                SqlCommand cmd1 = new SqlCommand("select * from Registration where id=@ID", con);
                cmd1.Parameters.AddWithValue("@ID", Session["id"]);
                SqlDataAdapter sda = new SqlDataAdapter();
                con.Open();
                sda.SelectCommand = cmd1;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                con.Close();
                if (dt.Rows.Count > 0)
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Registration set Name=@Name,Mobile=@Mobile WHERE Id=@ID", con);
                    if (checkForSQLInjection(txtName.Text.Trim()) || checkForSQLInjection(txtMobile.Text.Trim()))
                    {
                        lblMessage.Text = "Invalid Input data";
                        return;
                    }
                    checkForSQLInjection(txtName.Text.Trim());
                    checkForSQLInjection(txtMobile.Text.Trim());
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text.Trim());
                    cmd.Parameters.AddWithValue("@ID", Session["id"]);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    btnReset_OnClick(sender, e);
                }
            }
            else
            {
                SqlCommand cmd1 = new SqlCommand("select * from Registration where Name=@Name", con);
                cmd1.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                SqlDataAdapter sda = new SqlDataAdapter();
                con.Open();
                sda.SelectCommand = cmd1;
                DataTable dt1 = new DataTable();
                sda.Fill(dt1);
                con.Close();
                if (dt1.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('Name is already exist')", true);
                    btnReset_OnClick(sender, e);
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("insert into Registration (Name,Mobile) values (@Name,@Mobile)", con);
                    if (checkForSQLInjection(txtName.Text.Trim()) || checkForSQLInjection(txtMobile.Text.Trim()))
                    {
                        lblMessage.Text = "Invalid Input data";
                        return;
                    }
                    checkForSQLInjection(txtName.Text.Trim());
                    checkForSQLInjection(txtMobile.Text.Trim());
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text.Trim());
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    btnReset_OnClick(sender, e);
                }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Something Bad Happened";
        }
    }
    protected void btnReset_OnClick(object sender, EventArgs e)
    {
        txtName.Text = "";
        txtMobile.Text = "";
        lblMessage.Text = "";
        Session["id"] = null;
        Session["Edit"] = null;
        bind();
    }
    protected void grddisplay_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string ss = e.CommandArgument.ToString();
            Session["id"] = ss;
            if (e.CommandName == "edit")
            {
                SqlCommand cmd = new SqlCommand("select * from Registration where id=@ID", con);
                cmd.Parameters.AddWithValue("@ID", ss);
                SqlDataAdapter sda = new SqlDataAdapter();
                con.Open();
                sda.SelectCommand = cmd;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                con.Close();
                if (dt.Rows.Count > 0)
                {
                    txtName.Text = dt.Rows[0]["Name"].ToString();
                    txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
                    Session["Edit"] = "Edit";
                }
            }
            if (e.CommandName == "delete")
            {
                SqlCommand cmd = new SqlCommand("delete from Registration where id=@ID", con);
                cmd.Parameters.AddWithValue("@ID", ss);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                btnReset_OnClick(sender, e);
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('Data deleted successfully')", true);
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Something Bad Happened";
        }
    }
    public static Boolean checkForSQLInjection(string userInput)
    {
        bool isSQLInjection = false;
        string[] sqlCheckList = { "--",

                                       ";--",

                                       ";",

                                       "/*",

                                       "*/",

                                        "@@",

                                        "@",

                                        "char",

                                       "nchar",

                                       "varchar",

                                       "nvarchar",

                                       "alter",

                                       "begin",

                                       "cast",

                                       "create",

                                       "cursor",

                                       "declare",

                                       "delete",

                                       "drop",

                                       "end",

                                       "exec",

                                       "execute",

                                       "fetch",

                                            "insert",

                                          "kill",

                                             "select",

                                           "sys",

                                            "sysobjects",

                                            "syscolumns",

                                           "table",

                                           "update"

                                       };

        string CheckString = userInput.Replace("'", "''");
        for (int i = 0; i <= sqlCheckList.Length - 1; i++)
        {
            if ((CheckString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0))

            { isSQLInjection = true; }
        }

        return isSQLInjection;
    }

    protected void grddisplay_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void grddisplay_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

}